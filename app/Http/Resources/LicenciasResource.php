<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LicenciasResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'fechaEmision' => $this->fechaEmision,
            'fechaCaducidad' => $this->fechaEmision,
            'nombresApellidos' => $this->fechaEmision,
            'dni' => $this->fechaEmision,
            'nroLicencia' => $this->fechaEmision,
            'archivodni' => $this->fechaEmision,
            'archivonroLicencia' => $this->fechaEmision,
            'status' => $this->fechaEmision
        ];
    }
}