<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mainbanner;
use App\Models\Mainpage;
use App\Models\Quehacemos;
use App\Models\Repositorio;
use App\Models\Blog;
use App\Models\Clientes;
use App\Models\Equipo;
use App\Models\Anuncios;
use App\Http\Resources\RepositorioResource;

class GeneralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banner = Mainbanner::all();
        $general = Mainpage::all();
        $hacemos = Quehacemos::all();
        $repositorio = RepositorioResource::collection(Repositorio::all());
        $blogs = Blog::all();
        $clientes = Clientes::all();
        $equipo = Equipo::all();
        $anuncios = Anuncios::all();
        return response()->json([
            'banner'=>$banner,
            'general'=>$general,
            'hacemos'=>$hacemos,
            'repositorio'=>$repositorio,
            'blogs'=>$blogs,
            'clientes'=>$clientes,
            'equipo'=>$equipo,
            'anuncios'=>$anuncios
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
