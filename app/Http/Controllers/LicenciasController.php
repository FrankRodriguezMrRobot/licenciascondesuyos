<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Licencias;
use App\Http\Resources\LicenciasResource as LicenciasResource;

class LicenciasController extends Controller
{
    public function show($id)
    {
        $licencia = Licencias::findOrFail($id);
        return new LicenciasResource($licencia);
    }
    public function buscar(Request $request)
    {
        switch ($request->tipo) {
            case "id":
                $licencia = Licencias::findOrFail($request->id);
                return response()->json($licencia, 200); 
                break;
            case "dni":
                $licencia = Licencias::where('dni', 'LIKE', '%'.$request->dni.'%')->get();
                // $licencia = Licencias::where("dni","=",$request->dni)->get()[0];
                return response()->json($licencia, 200); 
                break;
            case "nroLicencia":
                $licencia = Licencias::where('nroLicencia', 'LIKE', '%'.$request->nroLicencia.'%')->get();
                // $licencia = Licencias::where("nroLicencia","=",$request->nroLicencia)->get()[0];
                return response()->json($licencia, 200); 
                break;
            case "nombresApellidos":
                $licencia = Licencias::where('nombresApellidos', 'LIKE', '%'.$request->nombresApellidos.'%')->get();
                // $licencia = Licencias::where("nombresApellidos","=",$request->nombresApellidos)->get()[0];
                return response()->json($licencia, 200); 
                break;
        }
        // $licencia = Licencias::findOrFail($id);
        // return response()->json(['licencia' => $licencia], 200); 
    }
}
