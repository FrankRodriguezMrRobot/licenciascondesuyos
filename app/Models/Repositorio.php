<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Repositorio extends Model
{
    protected $table = 'repositorios';
    public $timestamps = true;
    protected $fillable = [
        'nombre',
        'descripcion',
        'status',
        'archivos'
    ];
    use HasFactory;
}
