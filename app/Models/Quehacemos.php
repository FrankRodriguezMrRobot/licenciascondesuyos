<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quehacemos extends Model
{
    protected $table = 'quehacemos';
    public $timestamps = true;
    protected $fillable = [
        'titulo',
        'descripcion',
        'imagen',
        'status'
    ];
    use HasFactory;
}
