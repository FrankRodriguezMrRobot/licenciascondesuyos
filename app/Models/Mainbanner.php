<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mainbanner extends Model
{
    protected $table = 'mainbanners';
    public $timestamps = true;
    protected $fillable = [
        'titulo',
        'descripcion',
        'link',
        'imagen',
        'status'
    ];
    use HasFactory;
}
