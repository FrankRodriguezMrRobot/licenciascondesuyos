<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Equipo extends Model
{
    protected $table = 'equipos';
    public $timestamps = true;
    protected $fillable = [
        'titulo',
        'descripcion',
        'imagen',
        'status'
    ];
    use HasFactory;
}
