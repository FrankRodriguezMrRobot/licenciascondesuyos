<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dominios extends Model
{
    protected $table = 'dominios';
    public $timestamps = true;
    protected $fillable = [
        'nombre',
        'imagen',
        'posicion',
        'precio',
        'status'
    ];
    use HasFactory;
}
