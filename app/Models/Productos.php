<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
    protected $table = 'productos';
    public $timestamps = true;
    protected $fillable = [
        'nombre',
        'subtitulo',
        'precio',
        'periodo',
        'descripcion',
        'tipo',
        'imagen',
        'status'
    ];
    use HasFactory;
}