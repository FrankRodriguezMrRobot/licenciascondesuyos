<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Licencias extends Model
{
    use SoftDeletes;
    protected $table = 'licencias';
    public $timestamps = true;
    protected $fillable = [
        'fechaEmision',
        'fechaCaducidad',
        'nombresApellidos',
        'dni',
        'nroLicencia',
        'categoria',
        'archivodni',
        'archivodniReves',
        'archivonroLicencia',
        'archivonroLicenciaReves',
        'status'
    ];
    use HasFactory;
}