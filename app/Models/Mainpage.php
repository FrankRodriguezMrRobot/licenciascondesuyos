<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mainpage extends Model
{
    protected $table = 'mainpages';
    public $timestamps = true;
    protected $fillable = [
        'quienessomos',
        'quienessomosImage',
        'mercado',
        'mercadoImage'
    ];
    use HasFactory;
}
