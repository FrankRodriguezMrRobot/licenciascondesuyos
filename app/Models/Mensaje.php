<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mensaje extends Model
{
    protected $table = 'mensajes';
    public $timestamps = true;
    protected $fillable = [
        'nombresapellidos',
        'correo',
        'celular',
        'mensaje'
    ];
    use HasFactory;
}
