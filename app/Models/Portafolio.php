<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Portafolio extends Model
{
    protected $table = 'portafolios';
    public $timestamps = true;
    protected $fillable = [
        'nombre',
        'descripcion',
        'imagen',
        'status'
    ];
    use HasFactory;
}
