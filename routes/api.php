<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DataController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\ClientesController;
use App\Http\Controllers\EquipoController;
use App\Http\Controllers\MainBannerController;
use App\Http\Controllers\MainPageController;
use App\Http\Controllers\QuehacemosController;
use App\Http\Controllers\RepositorioController;
use App\Http\Controllers\GeneralController;
use App\Http\Controllers\AnunciosController;
use App\Http\Controllers\MensajesController;
use App\Http\Controllers\TerminosCondicionesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Route::apiResources([
// 	'data' => DataController::class,
//     'blog' => BlogController::class,
//     'cliente' => ClientesController::class,
//     'equipo' => EquipoController::class,
//     'banner' => MainBannerController::class,
//     'general' => MainPageController::class,
//     'hacemos' => QuehacemosController::class,
//     'repositorio' => RepositorioController::class,
//     'general' => GeneralController::class,
//     'anuncios' => AnunciosController::class,
//     'mensaje' => MensajesController::class,
//     'terminoscondiciones' => TerminosCondicionesController::class
// ]);
Route::post('buscar', 'App\Http\Controllers\LicenciasController@buscar');

// Route::middleware('auth:sanctum')->get('/user', function (Requpest $request) {
//     return $request->user();
// });
