<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLicenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('licencias', function (Blueprint $table) {
            $table->id();
            $table->date('fechaEmision');
            $table->date('fechaCaducidad');
            $table->string('nombresApellidos');
            $table->string('dni');
            $table->string('nroLicencia');
            $table->string('categoria')->nullable();
            $table->string('archivodni')->nullable();
            $table->string('archivodniReves')->nullable();
            $table->string('archivonroLicencia')->nullable();
            $table->string('archivonroLicenciaReves')->nullable();
            $table->boolean('status')->default(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('licencias');
    }
}