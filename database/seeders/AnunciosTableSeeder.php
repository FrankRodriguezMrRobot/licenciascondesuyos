<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AnunciosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('anuncios')->delete();
        
        \DB::table('anuncios')->insert(array (
            0 => 
            array (
                'id' => 1,
                'imagen' => NULL,
                'descripcion' => 'anuncio1',
                'status' => 1,
                'created_at' => '2021-09-24 23:17:37',
                'updated_at' => '2021-09-24 23:17:37',
            ),
            1 => 
            array (
                'id' => 2,
                'imagen' => NULL,
                'descripcion' => 'anuncio2',
                'status' => 1,
                'created_at' => '2021-09-24 23:17:44',
                'updated_at' => '2021-09-24 23:17:44',
            ),
            2 => 
            array (
                'id' => 3,
                'imagen' => NULL,
                'descripcion' => 'anuncio3',
                'status' => 1,
                'created_at' => '2021-09-24 23:17:51',
                'updated_at' => '2021-09-24 23:17:51',
            ),
            3 => 
            array (
                'id' => 4,
                'imagen' => NULL,
                'descripcion' => 'anuncio4',
                'status' => 1,
                'created_at' => '2021-09-24 23:17:58',
                'updated_at' => '2021-09-24 23:17:58',
            ),
            4 => 
            array (
                'id' => 5,
                'imagen' => NULL,
                'descripcion' => 'anuncio5',
                'status' => 1,
                'created_at' => '2021-09-24 23:18:04',
                'updated_at' => '2021-09-24 23:18:04',
            ),
            5 => 
            array (
                'id' => 6,
                'imagen' => NULL,
                'descripcion' => 'anuncio6',
                'status' => 1,
                'created_at' => '2021-09-24 23:18:10',
                'updated_at' => '2021-09-24 23:18:10',
            ),
            6 => 
            array (
                'id' => 7,
                'imagen' => NULL,
                'descripcion' => 'anuncio7',
                'status' => 1,
                'created_at' => '2021-09-24 23:18:16',
                'updated_at' => '2021-09-24 23:18:16',
            ),
            7 => 
            array (
                'id' => 8,
                'imagen' => NULL,
                'descripcion' => 'anuncio8',
                'status' => 1,
                'created_at' => '2021-09-24 23:18:22',
                'updated_at' => '2021-09-24 23:18:22',
            ),
            8 => 
            array (
                'id' => 9,
                'imagen' => NULL,
                'descripcion' => 'anuncio9',
                'status' => 1,
                'created_at' => '2021-09-24 23:18:28',
                'updated_at' => '2021-09-24 23:18:28',
            ),
        ));
        
        
    }
}