<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MainbannersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('mainbanners')->delete();
        
        \DB::table('mainbanners')->insert(array (
            0 => 
            array (
                'id' => 1,
                'titulo' => 'Banner 1',
                'descripcion' => 'banner uno banner uno banner uno banner uno banner uno banner uno banner uno banner uno',
                'imagen' => 'mainbanners/September2021/jVnHHRMh4tOvZ00yNdPF.jpg',
                'status' => 1,
                'created_at' => '2021-09-23 18:37:00',
                'updated_at' => '2021-09-24 23:59:58',
            ),
            1 => 
            array (
                'id' => 2,
                'titulo' => 'banner 2',
                'descripcion' => 'banner uno banner uno banner uno banner uno banner uno banner uno banner uno banner uno',
                'imagen' => 'mainbanners/September2021/eCEfUy3Rs4h1KLGs0ube.jpg',
                'status' => 1,
                'created_at' => '2021-09-23 18:37:00',
                'updated_at' => '2021-09-25 00:00:09',
            ),
            2 => 
            array (
                'id' => 3,
                'titulo' => 'banner 3',
                'descripcion' => 'banner uno banner uno banner uno banner uno banner uno banner uno banner uno banner uno',
                'imagen' => 'mainbanners/September2021/aWLFNrc0my58dFceEudl.jpg',
                'status' => 1,
                'created_at' => '2021-09-23 18:37:00',
                'updated_at' => '2021-09-24 23:59:49',
            ),
        ));
        
        
    }
}