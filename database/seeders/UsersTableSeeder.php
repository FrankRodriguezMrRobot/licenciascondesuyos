<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'role_id' => 1,
                'name' => 'admin',
                'email' => 'frank93rodriguez@gmail.com',
                'avatar' => 'users/default.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$N3sk86jd/HtWfNI3PUEg8e.thUn2StxMVuWr6XqOgC6IqlSQgUjqq',
                'remember_token' => NULL,
                'settings' => '{"locale":"en"}',
                'created_at' => '2021-09-19 18:18:02',
                'updated_at' => '2022-03-01 17:27:29',
            ),
            1 => 
            array (
                'id' => 2,
                'role_id' => 2,
                'name' => 'Rubén Rendon',
                'email' => 'dabrru_16_12_4@hotmail.com',
                'avatar' => 'users\\March2022\\OguMoaCm5e8msHapvoj4.png',
                'email_verified_at' => NULL,
                'password' => '$2y$10$pQKqQ9fTKX4Xwz2LSwYw.Olk73m5PU9MMqBqOJ1ybnyGTri6CYTbK',
                'remember_token' => NULL,
                'settings' => '{"locale":"en"}',
                'created_at' => '2022-03-01 17:56:54',
                'updated_at' => '2022-03-01 19:16:37',
            ),
        ));
        
        
    }
}