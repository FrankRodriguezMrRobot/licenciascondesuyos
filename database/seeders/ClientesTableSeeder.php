<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ClientesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('clientes')->delete();
        
        \DB::table('clientes')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'San Pablo',
                'imagen' => 'clientes/September2021/puUgYPpwlW8aq22RoMry.jpg',
                'status' => 1,
                'created_at' => '2021-09-23 18:38:00',
                'updated_at' => '2021-09-25 00:01:57',
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'El Pueblo',
                'imagen' => 'clientes/September2021/IfnMcSS5bO7xpCpg6frb.jpg',
                'status' => 1,
                'created_at' => '2021-09-23 18:38:00',
                'updated_at' => '2021-09-25 00:01:49',
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Alferza',
                'imagen' => 'clientes/September2021/hxAS9YhwVJWjbEcUxpwP.jpg',
                'status' => 1,
                'created_at' => '2021-09-23 18:38:00',
                'updated_at' => '2021-09-25 00:01:39',
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'UNSA',
                'imagen' => 'clientes/September2021/YTKOEv9xped1EscPfYjU.jpg',
                'status' => 1,
                'created_at' => '2021-09-23 18:39:00',
                'updated_at' => '2021-09-25 00:01:30',
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'GloVal',
                'imagen' => 'clientes/September2021/lFBcJzwvU5t4fjSg9P9Z.jpg',
                'status' => 1,
                'created_at' => '2021-09-23 18:39:00',
                'updated_at' => '2021-09-25 00:01:03',
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'Hanns Seidel Stiftung',
                'imagen' => 'clientes/September2021/2W1ZRPgbFIyCZSYXTQZI.jpg',
                'status' => 1,
                'created_at' => '2021-09-23 18:39:00',
                'updated_at' => '2021-09-25 00:00:54',
            ),
            6 => 
            array (
                'id' => 7,
                'nombre' => 'aurum',
                'imagen' => 'clientes/September2021/28NKTwoC5eK2NuYhU2hy.jpg',
                'status' => 1,
                'created_at' => '2021-09-23 18:40:00',
                'updated_at' => '2021-09-25 00:00:46',
            ),
        ));
        
        
    }
}