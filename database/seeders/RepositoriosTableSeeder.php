<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RepositoriosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('repositorios')->delete();
        
        \DB::table('repositorios')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Repositorio 1',
                'archivos' => '[{"download_link":"repositorios\\/September2021\\/6xPxdCR09BjVbYD6yobJ.pdf","original_name":"deploy-git-app.pdf"}]',
                'status' => 1,
                'created_at' => '2021-09-23 18:41:00',
                'updated_at' => '2021-09-25 16:52:18',
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Repositorio 2',
                'archivos' => '',
                'status' => 1,
                'created_at' => '2021-09-23 18:41:46',
                'updated_at' => '2021-09-23 18:41:46',
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'Repositorio 3',
                'archivos' => '',
                'status' => 1,
                'created_at' => '2021-09-23 18:41:00',
                'updated_at' => '2021-09-23 18:42:05',
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'Repositorio 4',
                'archivos' => '',
                'status' => 1,
                'created_at' => '2021-09-23 18:41:57',
                'updated_at' => '2021-09-23 18:41:57',
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'Repositorio 5',
                'archivos' => '',
                'status' => 1,
                'created_at' => '2021-09-23 18:42:11',
                'updated_at' => '2021-09-23 18:42:11',
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'Repositorio 6',
                'archivos' => '',
                'status' => 1,
                'created_at' => '2021-09-23 18:42:19',
                'updated_at' => '2021-09-23 18:42:19',
            ),
        ));
        
        
    }
}