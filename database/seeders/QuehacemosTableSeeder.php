<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class QuehacemosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('quehacemos')->delete();
        
        \DB::table('quehacemos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'titulo' => 'Estudios de mercado',
                'descripcion' => '-Investigacion de mercados

-Estudios de oferta y demanda

-Estudios situacionales y de diagnostico

-Pruebas de producto',
                'imagen' => 'quehacemos/September2021/8A0BQBEdqw2QSuTjaZjA.jpg',
                'status' => 1,
                'created_at' => '2021-09-23 18:32:00',
                'updated_at' => '2021-09-24 23:59:32',
            ),
            1 => 
            array (
                'id' => 2,
                'titulo' => 'Encuestas',
                'descripcion' => '-Elaboracion de cuestionarios

-Elaboracion de cuestionarios y levantamiento de informacion en campo

-Elaboracion de instrumento, levantamiento de informacion y tabulacion de encuestas

-Tabulacion de encuestas',
                'imagen' => 'quehacemos/September2021/w9l9e4slrY7EZTbTXYfR.jpg',
                'status' => 1,
                'created_at' => '2021-09-23 18:33:00',
                'updated_at' => '2021-09-24 23:59:23',
            ),
            2 => 
            array (
                'id' => 3,
                'titulo' => 'Estudios multicilente',
                'descripcion' => '-Estudios sindicados o estudios masivos que estan dirigidos a empresas que operan en un mismo sector, consisten en la investigacion de comportamientos de diferentes publicos objetivos y aspectos importantes del sector en el que opera.',
                'imagen' => 'quehacemos/September2021/x6b6QNaPz241wO0XhKTK.jpg',
                'status' => 1,
                'created_at' => '2021-09-23 18:33:00',
                'updated_at' => '2021-09-24 23:59:13',
            ),
            3 => 
            array (
                'id' => 4,
                'titulo' => 'Sondeos de opinion',
                'descripcion' => '-Recoleccion de informacion sobre opinion publica de un grupo de personas; involucra una muestra de participantes que representa una poblacion mas grande',
                'imagen' => 'quehacemos/September2021/WH5bxHxPMUD5MSngFiEL.jpg',
                'status' => 1,
                'created_at' => '2021-09-23 18:33:00',
                'updated_at' => '2021-09-24 23:59:03',
            ),
            4 => 
            array (
                'id' => 5,
                'titulo' => 'Focus group',
                'descripcion' => '-Elaboracion de dinamica grupal.

-Reclutamiento de participantes.

-Moderacion

-Realizacion

-Grabacion',
                'imagen' => 'quehacemos/September2021/9MR90wn1dAkOStjjg9iw.jpg',
                'status' => 1,
                'created_at' => '2021-09-23 18:33:00',
                'updated_at' => '2021-09-24 23:58:53',
            ),
            5 => 
            array (
                'id' => 6,
                'titulo' => 'Focus group',
                'descripcion' => '-Asesoria de investigacion de mercados

-Asesoria de levantamiento de informacion

-Asesoria en planeamiento estrategico',
                'imagen' => 'quehacemos/September2021/Bx0qDstxJGe5ZSJWOBPW.jpg',
                'status' => 1,
                'created_at' => '2021-09-23 18:34:00',
                'updated_at' => '2021-09-24 23:58:42',
            ),
        ));
        
        
    }
}