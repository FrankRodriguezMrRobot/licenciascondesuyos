<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('menus')->delete();
        
        \DB::table('menus')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'created_at' => '2021-09-19 18:19:19',
                'updated_at' => '2021-09-19 18:19:19',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Normal User',
                'created_at' => '2022-03-01 18:01:15',
                'updated_at' => '2022-03-01 18:01:15',
            ),
        ));
        
        
    }
}