<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MainpagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('mainpages')->delete();
        
        \DB::table('mainpages')->insert(array (
            0 => 
            array (
                'id' => 2,
                'quienessomos' => 'Ant Work Datos y Mercado es una empresa joven que viene operando desde 2019 en la ciudad de Arequipa, Perú. En Ant Work Datos & Mercado, estamos encargados de facilitar la información necesaria y confiable para que sus decisiones sean las correctas.',
                'quienessomosImage' => 'mainpages/September2021/reeVu9gkwKlKVT4DDgMp.jpg',
                'mercado' => 'Ant Work Datos y Mercado
n1 mdSoein sgaoborseil  · 
Sabías que algunos peruanos se llaman "Dios", "Misa", "Biblia", aquí te traemos los nombres más curiosos relacionados a la semana santa.
La información es tu mejor arma ⚔️
#arequipa #peru #EstudiosDeMercado #InvestigacionDeMercado #recopilaciondeDatos #datos #data #encuestas #semanaSanta #nombres


Ant Work Datos y Mercado
3Su0 dpe sfomaaruzio  · 
ANALIZAR, VISUALIZAR Y APLICAR lo que lógranos con la investigación de mercado. 
La información es tu mejor arma ⚔️
#arequipa #peru #EstudiosDeMercado #InvestigacionDeMercado #recopilaciondeDatos #datos #data #encuestas.',
                'mercadoImage' => 'mainpages/September2021/CXBs1lWJRv4RjRiP4L0n.jpg',
                'created_at' => '2021-09-25 17:14:00',
                'updated_at' => '2021-09-25 18:07:25',
            ),
        ));
        
        
    }
}