<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TerminosCondicionesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('terminos_condiciones')->delete();
        
        \DB::table('terminos_condiciones')->insert(array (
            0 => 
            array (
                'id' => 1,
                'titulo' => 'Términos y condiciones del uso de mis datos',
                'subtitulo' => NULL,
                'contenido' => '1. OBJETIVO.
Garantizar el cumplimiento de lo dispuesto en la Ley 1581 de 2012 y el Decreto
1377 de 2013, mediante la definición de lineamientos para el tratamiento y
protección de datos personales de los ciudadanos, que se encuentren a cargo de la Contraloría Departamental del Valle del Cauca. La Política de será informada a todos los titulares de los datos recolectados o que en el futuro se obtengan en el ejercicio de las actividades de consulta y manejo de la información generada, producida o difundida
por la Entidad.

2. RESPONSABLE.
Es responsabilidad de la Contraloría Departamental del Valle del Cauca, como persona jurídica así como de los directivos y servidores públicos velar por el cumplimiento de la presente política de tratamiento y protección de datos personales.

3. ALCANCE.
La Política de Tratamiento y Protección de datos Personales presentada a
continuación, se aplicará a toda la información recolectada por la entidad a través de cualquiera de los siguiente medios: Físico, electrónico o digital, bases de datos y demás Archivos que contengan datos personales y que sean objeto de tratamiento por la Contraloría Departamental del Valle del Cauca, considerados como datos personales.

4. TÉRMINOS Y DEFINICIONES.
Autorizaciones: Es el consentimiento previo, expreso e informado del Titular de
la información para llevar a cabo el Tratamiento de datos personales
Anonimización: Es el proceso que impide la identificación de las unidades de
estudio que son fuente para los registros individuales del conjunto de micro
datos.
Aviso de Privacidad: Es el instrumento (Anexo 2) a través del cual se le
comunica al titular de la información que la entidad cuenta con las políticas de
Tratamiento de información que le serán aplicables; éste debe ser entregado a
más tardar “al momento de la recolección de los datos personales”. Debe incluir
mínimo lo siguiente:

Datos del responsable del tratamiento.
Derechos del titular.
Canales dispuestos para que el titular conozca la política de tratamiento de datos personales de la Contraloría Departamental del Valle del Cauca.
Base de datos o Banco de Datos: Se entiende como el conjunto de datos
personales pertenecientes a un mismo contexto y almacenados
sistemáticamente para su posterior uso en cualquier medio (Papel, Electrónico,
Digital).

Contratos de Transmisión de Datos: Es el contrato que suscribe el
Responsable con los Encargados para el tratamiento de datos personales bajo
su control y responsabilidad, el cual señalará los alcances del tratamiento, las
actividades que el encargado realizará por cuenta del responsable para el
tratamiento de los datos personales y las obligaciones del Encargado para con
el Titular y el Responsable. Mediante dicho contrato el Encargado se
comprometerá a dar aplicación a las obligaciones del Responsable bajo la
política de tratamiento de la información fijada por este y a realizar el tratamiento
de datos de acuerdo con la finalidad que los Titulares hayan autorizado y con las
leyes aplicables.

Datos personales: Hace referencia a cualquier información vinculada o que
pueda asociarse a una o varias personas naturales determinadas. La
información pueden ser clasificada en cuatro grandes categorías: públicos,
semiprivados, privados y sensibles.

Datos públicos: Son todos aquellos que no son de naturaleza semiprivada o
privada, como también los contenidos en documentos públicos, sentencias
judiciales debidamente ejecutoriadas que no estén sometidas a reserva, y los
relativos al estado civil de las personas. Entre los datos de naturaleza pública a
resaltar se encuentran los siguientes ejemplos: los registros civiles de
nacimiento, matrimonio y defunción, y las cédulas de ciudadanía apreciadas de
manera individual y sin estar vinculadas a otro tipo de información.

Dato semiprivado: Es aquella información que no es de naturaleza íntima,
reservada ni pública y cuyo conocimiento o divulgación puede interesar no sólo
a su titular sino a cierto sector o grupo de personas o a la sociedad en general,
a manera de ejemplo, son los datos financieros, crediticios o actividades
comerciales.

Dato privado: Es la información de naturaleza íntima o reservada que por
encontrarse en un ámbito privado, sólo puede ser obtenida y ofrecida por orden
de autoridad judicial en el cumplimiento de sus funciones, así como por decisión
del titular de los mismos. Ejemplo de este tipo de datos son, los libros de los
comerciantes, de los documentos privados, de las historias clínicas o de la
información extraída a partir de la inspección del domicilio.

Datos sensibles: Es la información que afecta la intimidad del titular o cuyo uso
indebido puede generar su discriminación, tal es el caso del origen racial o étnico,
la orientación política, las convicciones religiosas o filosóficas, la pertenencia a sindicatos,
organizaciones sociales, de derechos humanos o que promueva intereses de cualquier
partido político o que garanticen los derechos y garantías de partidos políticos de oposición
así como los datos relativos a la salud, a la vida sexual y los datos biométricos.
Habeas data: Es el derecho que tiene toda persona para conocer, actualizar y
rectificar toda aquella información que se relacione con ella y que se recopile o
almacene en centrales de información.

Tratamiento de datos o de información: Cualquier operación o conjunto de
operaciones sobre datos personales, tales como la recolección,
almacenamiento, uso, circulación o supresión.

Encargado del Tratamiento: Persona natural o jurídica, pública o privada, que
por sí misma o en asocio con otros, realice el Tratamiento de datos personales
por cuenta del Responsable del Tratamiento.
Responsable del Tratamiento: Persona natural o jurídica, pública o privada,
que por sí misma o en asocio con otros, decida sobre la base de datos y/o el
Tratamiento de los mismos.

Titular: Persona natural cuyos datos personales sean objeto de Tratamiento.
Información: Se refiere a un conjunto organizado de datos contenido en
cualquier documento que los responsables y/o encargados del tratamiento
generen, obtengan, adquieran, transformen o controlen.

Información pública: Es toda información que el responsable y/o encargado del
tratamiento, genere, obtenga, adquiera, o controle en su calidad de tal.
Información pública clasificada: Es aquella información que estando en poder
de un sujeto responsable en su calidad de tal, pertenece al ámbito propio,
particular y privado o semiprivado de una persona natural o jurídica, por lo que
su acceso podrá ser negado o exceptuado, siempre que se trate de las
circunstancias legítimas y necesarias y los derechos particulares o privados
consagrados en la ley.

Información pública reservada: Es aquella información que estando en poder
de un sujeto responsable en su calidad de tal, es exceptuada de acceso a la
ciudadanía por daño a intereses públicos.

Documento de Archivo: Es el registro de información producida o recibida por
una entidad pública o privada en razón de sus actividades o funciones.

Datos abiertos: Son todos aquello datos primarios o sin procesar, que se
encuentran en formatos estándar e interoperables que facilitan su acceso y
reutilización, los cuales están bajo la custodia de las entidades públicas o
privadas que cumplen con funciones públicas y que son puestos a disposición
de cualquier ciudadano, de forma libre y sin restricciones, con el fin de que
terceros puedan reutilizarlos y crear servicios derivados de los mismos.

5. CONTENIDO DE LA POLÍTICA.

5.1. TRATAMIENTO DE LOS DATOS PERSONALES
El tratamiento que realizará la Contraloría Departamental del Valle del Cauca será el de recolectar, almacenar, procesar, usar y transmitir o transferir (según corresponda) los datos personales de los ciudadanos, atendiendo de forma estricta los deberes de seguridad y confidencialidad ordenados por la Ley 1581 de 2012 y el Decreto 1377 de 2013, con las siguientes finalidades:

1) Registrar la información de datos personales en las bases de datos de la
Contraloría Departamental del Valle del Cauca, con la finalidad de analizar, evaluar y generar información estadística para el cumplimiento de los planes y programas a implementar por la administración.
2) Para el desarrollo de las funciones propias de la entidad.
3) Enviar la información a entidades gubernamentales o judiciales por solicitud
expresa de las mismas.
4) Caracterizar ciudadanos y grupos de interés y adelantar estrategias de
mejoramiento en la prestación del servicio.
5) Soportar procesos de auditoría externa e interna.
6) Garantizar el cumplimiento de los trámites y servicios que se ofrecen en la
entidad.
7) Dejar evidencia de las entregas de productos o servicios de los diferentes
programas o proyectos de la entidad dirigidos a la comunidad.

Así mismo, la Contraloría Departamental del Valle del Cauca entregara los datos personales a terceros que le provean servicios o con quien tenga algún tipo de relación de cooperación interinstitucional, conforme al artículo 6 de la ley 1377, con el fin de:

1) Brindar asistencia técnica.
2) Facilitar la implementación de programas en cumplimiento de mandatos
legales.
3) Manejar y administrar bases de datos.
4) Dar respuestas a peticiones, quejas y recursos.
5) Dar respuestas a organismos de control.  

Cuando la Contraloría Departamental del Valle del Cauca reciba información que le haya sido transferida por otras entidades descentralizadas debido a su solicitud, le dará el mismo tratamiento de confidencialidad y seguridad que le proporciona a la información producida por la administración.

En este sentido, la Contraloría Departamental del Valle del Cauca, orientara a las respectivas Unidades Administrativas a su cargo y entidades descentralizadas sobre;

Su calidad de responsables de tratamiento de los datos recaudados por ellas.
La vigilancia que deben ejercer en cuanto al respeto del habeas data en la relación
establecida entre los diferentes actores de comunidad y grupos de valor de acuerdo
a la naturaleza de cada ente descentralizado, secretaria u oficina asesora.
Conservación de la información bajo las condiciones de seguridad
necesarias para impedir su adulteración, pérdida, consulta, uso o acceso no
autorizado o fraudulento.
5.2. POLÍTICAS DE TRATAMIENTO DE DATOS PERSONALES.
Los funcionarios de la Contraloría Departamental del Valle del Cauca esta obligados a Cumplir con los siguientes lineamientos:

Cumplir con la normatividad legal vigente colombiana que dicte disposiciones
para la protección de datos personales.
Cumplir con la ley de protección de datos personales de acuerdo con lo
contemplado en el presente documento y en sus actualizaciones.
Los Servidores Públicos deben acogerse a las inhabilidades, impedimentos,
incompatibilidades y conflicto de intereses contemplados en la Ley 734 de
2002 (Código Disciplinario Único, capitulo cuarto) para el tratamiento de
Datos Personales.
Cada unidad administrativa (Secretaria y Oficinas Asesoras) de la entidad,
de acuerdo a sus competencias e información a su cargo, son los
responsables del tratamiento y del manejo de la información, Por lo tanto
deben establecer la finalidad y la forma en que se recolectan, almacenan y
administran los datos. Así mismo, están obligados a solicitar y conservar la
autorización en la que conste el consentimiento expreso del titular de la
información.
Cada unidad administrativa debe designar a un colaborador del área
responsable de la administración de la base de datos de acuerdo el manual
de funciones de la entidad o tipo de contrato, el cual debe estar articulado
con el área de sistemas de la entidad y acatar los protocolos de seguridad
emitidos por esta dependencia relacionado con el manejo constate de la
información.
El área encargada de los sistemas informáticos de la entidad, exigirá y
recibirá de cada unidad administrativa que cuente con una base de datos el
reglamento interno o protocolo que contenga las responsabilidades,
funciones del encargado de la administración de los datos y las
consecuencias en caso del mal manejo de la información. El responsable o
encargado de la base de datos es diferente al responsable y encargado del
tratamiento de los datos.
La Contraloría Departamental del Valle del Cauca, entenderá como comprendida y aceptada la política de tratamientos de datos personales por los ciudadanos interesados. Cuando acepten los términos de la política emitida en los avisos de privacidad, publicados en los formatos o en formularios web y sistemas disponibles por la entidad, para la recolección de la información. La Contraloría Departamental del Valle del Cauca solicitará la autorización a los Titulares de los datos personales y mantendrá las pruebas de ésta, cuando en virtud de las funciones de promoción, divulgación y capacitación, realice invitaciones a charlas, conferencias o eventos que impliquen el Tratamiento de Datos Personales con una finalidad diferente para la cual fueron recolectados inicialmente.
La Contraloría Departamental del Valle del Cauca autorizara mediante oficio o acto administrativo al responsable de la Administración de las bases de datos en cada unidad administrativa, para realizar el tratamiento solicitado por el Titular de la información.
La Contraloría Departamental del Valle del Cauca no publicara y tampoco colocara a disposición de terceros los Datos Personales, para su acceso a través de Internet u otros medios masivos de comunicación, a menos que se trate de información pública o que se establezcan medidas técnicas que permitan controlar el acceso y restringirlo solo a las personas autorizadas por ley o por el titular.
Todo Dato Personal que no sea Dato Público se tratará por la Contraloría Departamental del Valle del Cauca como confidencial, aun cuando la relación contractual o el vínculo entre el Titular del Dato Personal y la Contraloría Departamental del Valle del Cauca haya finalizado.

A la terminación de dicho vínculo, tales Datos Personales deben continuar
siendo Tratados de acuerdo con lo dispuesto por los lineamientos de grupo
de Archivo y correspondencia o el proceso de Gestión Documental de la
entidad.
Las políticas establecidas por la Contraloría Departamental del Valle del Cauca respecto al tratamiento de Datos Personales podrán ser modificadas en cualquier momento. Toda modificación se realizará conforme a la normatividad legal vigente, y las mismas entrarán en vigencia y tendrán efectos desde su publicación a través de los mecanismos dispuestos por la Contraloría Departamental del Valle del Cauca para que los titulares conozcan la política de tratamiento de la
información y los cambios que se produzcan en ella.
Los encargados del Tratamiento de información de Datos Personales, podrán
suministrar según el caso, la información de Datos Personales, únicamente cuando el tratamiento se realice en virtud de sus funciones legales y cuando excepcionalmente éstas no apliquen, lo debe realizar con autorización del Titular de los datos o información.
Los Secretarios y Jefes de Oficinas Asesoras deberán reportar
oportunamente las bases de datos nuevas y las novedades de las bases de datos existentes ante la superintendencia de industria comercio y demás entidades que en el marco jurídico lo exijan.
La Contraloría Departamental del Valle del Cauca implementara y actualizara procedimientos para garantizar el cumplimiento de las políticas de tratamientos de datos personales de la entidad.
5.3. DERECHOS DEL TITULAR DE LOS DATOS PERSONALES.

Como titular de datos personales, se tiene derecho a:

Acceder en forma gratuita a los datos proporcionados a la Contraloría Departamental del Valle del Cauca que hayan sido objeto de tratamiento. Asumiendo los costos de reproducción por el solicitante cuando se requiera.
Conocer, actualizar y rectificar sus datos personales, este derecho se podrá
ejercer también, frente a datos parciales, inexactos, incompletos,
fraccionados, que induzcan a error, o aquellos cuyo tratamiento esté
expresamente prohibido o no haya sido autorizado.
Presentar queja ante la Superintendencia de Industria y Comercio por
infracciones a lo dispuesto en la Ley 1581 de 2012 y las demás normas que
la modifiquen, adicionen o complementen, una vez haya agotado el trámite
de reclamo ante el responsable o encargado del tratamiento de datos
personales.
Solicitar la supresión del dato cuando en el tratamiento no se respeten los
principios, derechos y garantías constitucionales y legales, el cual procederá cuando la autoridad haya determinado que la Contraloría Departamental del Valle del Cauca en el tratamiento ha incurrido en conductas contrarias a la Constitución y la normatividad vigente.
Ser informado del uso y tratamiento dado a sus datos personales, previa
solicitud elevada a través de los canales de servicio.
Conocer la política de tratamiento de datos de la entidad y a través de ella, el uso o finalidad que se le dará a sus datos personales.
Identificar al responsable en la Contraloría Departamental del Valle del Cauca que dará trámite y respuesta a sus solicitudes.
Los demás señalados por el artículo 8 de la Ley 1581 de 2012.
5.4. DEBERES DE LA CONTRALORÍA DEPARTAMENTAL DEL VALLE DEL CAUCA COMO RESPONSABLE YENCARGADO DEL TRATAMIENTO DE LOS DATOS PERSONALES.

En el tratamiento y protección de datos personales, la Contraloría Departamental del Valle del Cauca tendrá los siguientes deberes, sin perjuicio de otros previstos en las disposiciones que reglamenten o lleguen a regular esta materia.

Garantizar al titular, en todo tiempo, el pleno y efectivo ejercicio del derecho
de hábeas data.
Solicitar y conservar copia de la respectiva autorización otorgada por el titular
para el tratamiento de datos personales.
Informar debidamente al titular sobre la finalidad de la recolección de los
datos o información y los derechos que le asisten en virtud de la autorización
otorgada.
Conservar la información bajo las condiciones de seguridad necesarias para
impedir su adulteración, pérdida, consulta, uso o acceso no autorizado o
fraudulento.
Garantizar que la información sea veraz, completa, exacta, actualizada,
comprobable y comprensible.
Actualizar oportunamente la información, atendiendo de esta forma todas las
novedades respecto de los datos del titular.
Adicionalmente, se deberán implementar todas las medidas necesarias para
que la información se mantenga actualizada.
Rectificar la información cuando sea incorrecta y comunicar lo pertinente.
Respetar las condiciones de seguridad y privacidad de la información del
titular.
Informar a la superintendencia de industria y comercio, cuando se presenten
violaciones a los códigos de seguridad y existan riesgos en la administración
de la información de los titulares.
Cumplir los requerimientos e instrucciones que imparta la Superintendencia
de Industria y Comercio sobre el tema en particular.
Usar únicamente datos cuyo tratamiento esté previamente autorizado de
conformidad con lo previsto en la Ley 1581 de 2012.
Garantizar el uso adecuado de los datos personales de los niños, niñas y
adolescentes, una vez autoricen el tratamiento de sus datos.
Permitir el acceso a la información únicamente a las personas que tienen
derecho a ella.
Abstenerse de circular información que esté siendo controvertida por el titular
y cuyo bloqueo haya sido ordenado por la Superintendencia de Industria y
Comercio
Usar los datos personales del titular solo para aquellas finalidades para las
que se encuentre facultada debidamente y respetando en todo caso la
normatividad vigente sobre protección de datos personales.
5.5. CREACIÓN DE LAS BASES DE DATOS.

Cuando una Unidad Administrativa de la Contraloría Departamental del Valle del Cauca requiera recolectar datos personales para crear una base de datos, debe identificar claramente la Finalidad de la recolección, el ¿Por qué? necesita esa información. Es necesario que la finalidad esté relacionada con las funciones atribuidas a la entidad en el área específica y debe ser incluida en el formato de autorización. (Anexo 1).
Cada Unidad administrativa debe identificar previamente los datos que solicitará y debe clasificarlos de la siguiente manera: Pública, semiprivada, privada y sensible, lo anterior teniendo en cuenta el aparte de definiciones contempladas para presente política.
Una vez la unidad administrativa defina la finalidad, debe establecer el periodo de tiempo dentro del cual se hará uso de la información, un límite temporal para utilizar la información, no es necesario establecer fechas exactas, se puede asociar a circunstancias o condiciones que agoten la finalidad.

5.6. REGISTRO DE LA BASE DE DATOS
La Contraloría Departamental del Valle del Cauca debe incluir las bases de datos creadas en el Registro Nacional de Bases de datos de la Superintendencia de Industria y Comercio, que debe contener la siguiente información:

Finalidad
Población a la cual se recopiló la información.
Descripción básica de los tipos de datos solicitados.
Área encargada de la administración de la base de datos.
Herramientas para que el titular de la información cancele, rectifique o
modifique sus datos personales.
Medidas de seguridad aplicable.
Adicionalmente debe aportar la política de tratamiento de datos personales
de la entidad.
5.7. CIERRE DE LA BASE DE DATOS
Para la Contraloría Departamental del Valle del Cauca, la recolección de la información debe establecer su finalidad, por lo tanto cuando ésta deja de existir no es procedente seguir con el tratamiento de los datos personales; no obstante, si el término que se había establecido inicialmente no es suficiente y se requiere prorrogarlo, es necesario contar nuevamente con la autorización del titular de la información para llevarlo a cabo.

Los Titulares de la información suministrada a la Contraloría Departamental del Valle del Cauca podrán en todo momento y cuando consideren que los datos no están recibiendo un tratamiento adecuado o no son pertinentes o necesarios para la finalidad para la cual fueron recolectados, solicitar a entidad la supresión de los datos personales mediante la presentación de un reclamo.

Para la Contraloría Departamental del Valle del Cauca, la solicitud de supresión de datos no procederá, cuando el titular tenga un deber legal o contractual de permanecer en la(s) base(s) de datos o la supresión de los datos represente un impedimento en actuaciones administrativas o judiciales relacionadas a obligaciones fiscales, investigación de delitos o actualización de sanciones administrativas. Si vencido el término legal respectivo, no se han eliminado los datos personales, el titular tendrá derecho a solicitar a la Superintendencia de Industria y Comercio que ordene la supresión de los datos personales.

La Contraloría Departamental del Valle del Cauca garantizara los medios de comunicación electrónica u otros para solicitudes de supresión de las bases de datos, que serán los mismos utilizados para la recepción y atención de peticiones, quejas, reclamos, sugerencias, denuncias y/o felicitaciones administrados por las áreas de atención al Ciudadano definidas por la entidad.

En la Contraloría Departamental del Valle del Cauca el proceso de anonimizar la información no sólo implica la eliminación de las variables de identificación directa de la unidad de observación (por ejemplo, la cédula o el NIT de una empresa), sino que se deben realizar las seis etapas que son: i) Revisiones previas; ii) Análisis de riesgos de identificación de las fuentes de información; iii) Identificación y selección de técnicas de anonimización; iv) Análisis de viabilidad del proceso, v) Aplicación de técnicas de anonimizació , y vi) Evaluación de resultados del proceso. Lo anterior para garantizar la confidencialidad de los datos.

5.8. ÁREA RESPONSABLE DE LA ATENCIÓN DE PETICIONES,
CONSULTAS Y RECLAMOS SOBRE DATOS PERSONALES.
La Contraloría Departamental del Valle del Cauca tiene a su cargo la labor de desarrollo, implementación, capacitación y seguimiento de esta Política por parte del jefe de la oficina de sistemas y su equipo trabajo.

La entidad ha dispuesto las áreas de atención al ciudadano de cada secretaria,
como responsables de la atención de peticiones, consultas, quejas y reclamos como canales ante las cuales los Titulares de los datos personales podrán ejercer sus derechos relacionados con actualizar, conocer y rectificar los datos o la información suministrada inicialmente. Para lo cual las unidades administrativas, mediante los servidores públicos responsables del tratamiento de datos personales, están obligados a atender las solicitudes peticiones, quejas o reclamos interpuestos por parte de los Titulares de los datos personales en los tiempos establecidos por la ley.


Asignadas o direccionados por las unidades de Atención al Ciudadano de la Contraloría Departamental del Valle del Cauca  (Secretaria de Educación, Secretaria de Salud, Secretaria de Hacienda, Secretaria General, Secretaria de Transito).

6. PROCEDIMIENTO PARA PARA LA ATENCIÓN Y RESPUESTA A
PETICIONES CONSULTAS QUEJAS Y RECLAMOS DE LOS TITULARES DE
DATOS PERSONALES.
Los Titulares de los Datos Personales que estén siendo recolectados, almacenados, procesados, usados y transmitidos o transferidos por la Contraloría Departamental del Valle del Cauca, podrán ejercer en cualquier momento sus derechos a conocer, actualizar y rectificar la información. Para el efecto, se seguirá el siguiente procedimiento, de conformidad con la Ley de Protección de Datos Personales:


6.1. Medios habilitados para la presentación de peticiones, consultas,
quejas y reclamos:
La Contraloría Departamental del Valle del Cauca tiene disponibles los siguientes medios para la recepción y atención de peticiones, consultas, quejas y reclamos que permiten conservar prueba de las mismas:

Comunicación escrita dirigida a la Contraloría Departamental del Valle del Cauca Unidad de Atención al Ciudadano, Carrera 6 entre calles 9 y 10 edificio Gobernación del Valle del Cauca piso 6
Comunicación telefónica: Número (57+2) 8822488 - 8881891 de lunes a viernes de 8:00 a.m. a 5:00 p.m.

6.2. Atención y respuesta a peticiones y consultas: El Titular o su apoderado,
podrán solicitara la Contraloría Departamental del Valle del Cauca:

Información sobre los Datos Personales del Titular que son objeto de
Tratamiento.
Información respecto del uso que se le ha dado por la Contraloría Departamental del Valle del Cauca a sus datos personales.
Las peticiones y consultas serán atendidas en un término máximo de quince
(15) días hábiles contados a partir de la fecha de recibo de las mismas.
Cuando no fuere posible atender la petición o consulta dentro de dicho
término, se informará al interesado, expresando los motivos de la demora y señalando cuando se atenderá su petición o consulta, la cual en ningún caso podrá superar los cinco (5) días hábiles siguientes al vencimiento del primer término.
6.3. Atención y respuesta a quejas y reclamos:

El Titular o sus apoderados, podrán solicitar a la Contraloría Departamental del Valle del Cauca, mediante queja o reclamo presentado por los canales ya indicados: La corrección o actualización de la información, que se subsane o corrija el presunto incumplimiento a cualquiera de los deberes contenidos en la Ley de Protección de Datos Personales.

La solicitud deberá contener como mínimo la descripción de los hechos que dan
lugar a la queja o reclamo, la dirección y datos de contacto del solicitante. Si la queja
o reclamo se presentan incompletos, la Contraloría Departamental del Valle del Cauca deberá requerir al interesado dentro de los cinco (5) días siguientes a la recepción de la queja o reclamo para que subsane las fallas. Transcurridos dos (2) meses desde la fecha del requerimiento, sin que el solicitante presente la información requerida, se entenderá que ha desistido de la queja o reclamo. En caso que la dependencia que reciba la queja o reclamo no sea competente para resolverla, deberá dar traslado a la Unidad de Atención al Ciudadano para que la remita al área que corresponda de la Contraloría Departamental del Valle del Cauca, en un término máximo de dos (2) días hábiles e informará de lo ocurrido al interesado.

El término máximo para atender la queja o el reclamo será de quince (15) días
hábiles contados a partir del día siguiente a la fecha de su recibo.',
'created_at' => '2021-09-29 16:57:42',
'updated_at' => '2021-09-29 16:57:42',
),
));
        
        
    }
}