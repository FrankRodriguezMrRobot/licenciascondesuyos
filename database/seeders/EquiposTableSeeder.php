<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class EquiposTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('equipos')->delete();
        
        \DB::table('equipos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'titulo' => 'Beny Palomino',
                'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.',
                'imagen' => 'equipos/September2021/fvF7XwFCYvdBXAl6sjN4.jpg',
                'status' => 1,
                'created_at' => '2021-09-23 18:40:00',
                'updated_at' => '2021-09-25 00:02:32',
            ),
            1 => 
            array (
                'id' => 2,
                'titulo' => 'Cesar Ventura',
                'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.',
                'imagen' => 'equipos/September2021/4l1aP3bDMENN8CJuIY3B.jpg',
                'status' => 1,
                'created_at' => '2021-09-23 18:41:00',
                'updated_at' => '2021-09-25 00:02:24',
            ),
            2 => 
            array (
                'id' => 3,
                'titulo' => 'Beny Palomino',
                'descripcion' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.',
                'imagen' => 'equipos/September2021/GSNx7hb6TPQKRyknttbJ.jpg',
                'status' => 1,
                'created_at' => '2021-09-23 18:41:00',
                'updated_at' => '2021-09-25 00:02:14',
            ),
        ));
        
        
    }
}