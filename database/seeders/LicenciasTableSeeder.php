<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use League\Csv\Reader;
use DB;

class LicenciasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //////////SEEDER LICENCIAS///////////////////////
        $csv = Reader::createFromPath('public/importacion/licencias.csv', 'r');
        $csv->setDelimiter(';');
        $csv->addStreamFilter('convert.iconv.ISO-8859-1/UTF-8');
        $csv->setHeaderOffset(0);
        $header = $csv->getHeader(); //returns the CSV header record
        $records = $csv->getRecords(); //returns all the CSV records as an Iterator object
        foreach ($csv as $record) {
            DB::table('licencias')->insert([
                'fechaEmision' => $record['fechaEmision'],
                'fechaCaducidad' => $record['fechaCaducidad'],
                'nombresApellidos' => $record['nombresApellidos'],
                'dni' => $record['dni'],
                'nroLicencia' => $record['nroLicencia'],
                'categoria' => str_replace(' ', '', $record['categoria']),//trim($record['categoria']),str_replace(' ', '', $record['categoria']);
                // 'archivodni' => "",
                // 'archivonroLicencia' => "",
                'status' => 1,
                "created_at" =>  \Carbon\Carbon::now(),
                "updated_at" => \Carbon\Carbon::now()
            ]);
        }
    }
}
