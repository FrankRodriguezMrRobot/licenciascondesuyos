<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Artisan;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(DataTypesTableSeeder::class);
        $this->call(DataRowsTableSeeder::class);
        $this->call(MenusTableSeeder::class);
        $this->call(MenuItemsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(PermissionRoleTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(TranslationsTableSeeder::class);



        Artisan::call('view:clear');
        Artisan::call('cache:clear');
        //MAIN STRUCTURE
        // $this->call(BlogsTableSeeder::class);
        // $this->call(RepositoriosTableSeeder::class);
        // $this->call(MainpagesTableSeeder::class);
        // $this->call(MainbannersTableSeeder::class);
        // $this->call(ClientesTableSeeder::class);
        // $this->call(EquiposTableSeeder::class);
        // $this->call(QuehacemosTableSeeder::class);
        // $this->call(AnunciosTableSeeder::class);
        // $this->call(TerminosCondicionesTableSeeder::class);
        $this->call(LicenciasTableSeeder::class);

    }
}
