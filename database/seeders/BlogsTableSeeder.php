<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class BlogsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('blogs')->delete();
        
        \DB::table('blogs')->insert(array (
            0 => 
            array (
                'id' => 1,
                'titulo' => 'Dia de san valentin',
                'descripcion' => 'La familia Ant Work les desea un Feliz San Valentín, esperamos que lo pasen lindo con su persona favorita 💗💓
La información es tu mejor arma ⚔️',
                'imagen' => 'blogs/September2021/UT0tYR3G9z4qTJnivqa6.jpg',
                'tipo' => 'reporte',
                'status' => 1,
                'created_at' => '2021-09-23 18:44:00',
                'updated_at' => '2021-09-25 00:05:18',
            ),
            1 => 
            array (
                'id' => 2,
                'titulo' => 'Arequipa y el uso de internet',
            'descripcion' => 'Sabías que un 71.9% de los arequipeños se conectan a internet por el internet fijo (wifi) ☝️

La información es tu mejor arma ⚔️',
                'imagen' => 'blogs/September2021/ClOyt06Xm2KoBv8L09G5.jpg',
                'tipo' => 'reporte',
                'status' => 1,
                'created_at' => '2021-09-23 18:45:00',
                'updated_at' => '2021-09-25 00:05:08',
            ),
            2 => 
            array (
                'id' => 3,
                'titulo' => '¿Cuenta con acceso a internet?',
                'descripcion' => 'Acceso a internet según el Nivel Socioeconómico
El sector A1 - A2 -B1 tienen acceso a internet al 100% 🧐👩‍💻

La información es tu mejor arma ⚔️',
                'imagen' => 'blogs/September2021/wBE3RW9FNUPmECoLhQVE.jpg',
                'tipo' => 'reporte',
                'status' => 1,
                'created_at' => '2021-09-23 18:45:00',
                'updated_at' => '2021-09-25 00:04:58',
            ),
            3 => 
            array (
                'id' => 4,
                'titulo' => 'Dia internacional de la mujer',
                'descripcion' => 'Hoy Día internacional de la Mujer, conmemoramos su lucha. ¿Sabias qué fuimos el penúltimo país de Latinoamérica en conceder el derecho al voto femenino?',
                'imagen' => 'blogs/September2021/EYFgrlQRYzMC9pAeKmoT.jpg',
                'tipo' => 'reporte',
                'status' => 1,
                'created_at' => '2021-09-23 18:45:00',
                'updated_at' => '2021-09-25 00:04:44',
            ),
            4 => 
            array (
                'id' => 5,
                'titulo' => 'Profesiones mas requeridas',
                'descripcion' => 'Sabias que la carrera de Administración de Empresas y Contabilidad y Finanzas son las más requeridas, aquí te traemos los 6 empleos más solicitados entre 2016 y 2018

La información es tu mejor arma ⚔️',
                'imagen' => 'blogs/September2021/LJncDagjqMw6Xy7h9Kpj.jpg',
                'tipo' => 'reporte',
                'status' => 1,
                'created_at' => '2021-09-23 18:45:00',
                'updated_at' => '2021-09-25 00:04:35',
            ),
            5 => 
            array (
                'id' => 6,
                'titulo' => 'Evolucion historica de la economia peruana',
                'descripcion' => 'Solo en el último año la economía peruana sufrió un descenso de un 11.1% 📉🔍

La información es tu mejor arma ⚔️',
                'imagen' => 'blogs/September2021/DxwS3MjngSqE03z1siae.jpg',
                'tipo' => 'reporte',
                'status' => 1,
                'created_at' => '2021-09-23 18:46:00',
                'updated_at' => '2021-09-25 00:04:25',
            ),
            6 => 
            array (
                'id' => 7,
                'titulo' => 'Macro region sur y Arequipa region',
                'descripcion' => 'La tasa de desempleo en la Macro región sur y Arequipa, fue de 3.0 y 3.8 por ciento respectivamente en el 2018. Por lo tanto, según su evolución histórica, se espera que en el 2019 haya fluctuado alrededor de ella. Sin embargo, esperamos al término del 2020, la tasa de desempleo se encuentre muy por encima de la media histórica, explicado por la paralización económica nacional, a causa de la Covid-2019.

La información es tu mejor arma ⚔️',
                'imagen' => 'blogs/September2021/Lyo4TFmkkHeBvlgGxdPH.jpg',
                'tipo' => 'reporte',
                'status' => 1,
                'created_at' => '2021-09-23 18:46:00',
                'updated_at' => '2021-09-25 00:04:13',
            ),
            7 => 
            array (
                'id' => 8,
                'titulo' => 'Remuneracion segun profesores',
                'descripcion' => 'Ingresos mensuales de trabajadores jóvenes que egresaron de la universidad entre 2016 y 2018
1. Medicina
2. Agronegocios
3. Ingeniería de Telecomunicaciones
4. Investigación operativa
5. Geología
6. Ingeniería Eléctrica

La información es tu mejor arma ⚔️',
                'imagen' => 'blogs/September2021/CEnue8U7ULusz4bYTCwJ.jpg',
                'tipo' => 'reporte',
                'status' => 1,
                'created_at' => '2021-09-23 18:46:00',
                'updated_at' => '2021-09-25 00:04:04',
            ),
            8 => 
            array (
                'id' => 9,
                'titulo' => 'Semana Santa - Tiempo de reflexion',
                'descripcion' => 'Sabías que algunos peruanos se llaman "Dios", "Misa", "Biblia", aquí te traemos los nombres más curiosos relacionados a la semana santa.
La información es tu mejor arma ⚔️',
                'imagen' => 'blogs/September2021/lKzn7HiOLnKGYy9kN1NW.jpg',
                'tipo' => 'reporte',
                'status' => 1,
                'created_at' => '2021-09-23 18:47:00',
                'updated_at' => '2021-09-25 00:03:53',
            ),
        ));
        
        
    }
}