<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'key' => 'browse_admin',
                'table_name' => NULL,
                'created_at' => '2021-09-19 18:19:19',
                'updated_at' => '2021-09-19 18:19:19',
            ),
            1 => 
            array (
                'id' => 2,
                'key' => 'browse_bread',
                'table_name' => NULL,
                'created_at' => '2021-09-19 18:19:19',
                'updated_at' => '2021-09-19 18:19:19',
            ),
            2 => 
            array (
                'id' => 3,
                'key' => 'browse_database',
                'table_name' => NULL,
                'created_at' => '2021-09-19 18:19:19',
                'updated_at' => '2021-09-19 18:19:19',
            ),
            3 => 
            array (
                'id' => 4,
                'key' => 'browse_media',
                'table_name' => NULL,
                'created_at' => '2021-09-19 18:19:19',
                'updated_at' => '2021-09-19 18:19:19',
            ),
            4 => 
            array (
                'id' => 5,
                'key' => 'browse_compass',
                'table_name' => NULL,
                'created_at' => '2021-09-19 18:19:19',
                'updated_at' => '2021-09-19 18:19:19',
            ),
            5 => 
            array (
                'id' => 6,
                'key' => 'browse_menus',
                'table_name' => 'menus',
                'created_at' => '2021-09-19 18:19:19',
                'updated_at' => '2021-09-19 18:19:19',
            ),
            6 => 
            array (
                'id' => 7,
                'key' => 'read_menus',
                'table_name' => 'menus',
                'created_at' => '2021-09-19 18:19:19',
                'updated_at' => '2021-09-19 18:19:19',
            ),
            7 => 
            array (
                'id' => 8,
                'key' => 'edit_menus',
                'table_name' => 'menus',
                'created_at' => '2021-09-19 18:19:19',
                'updated_at' => '2021-09-19 18:19:19',
            ),
            8 => 
            array (
                'id' => 9,
                'key' => 'add_menus',
                'table_name' => 'menus',
                'created_at' => '2021-09-19 18:19:19',
                'updated_at' => '2021-09-19 18:19:19',
            ),
            9 => 
            array (
                'id' => 10,
                'key' => 'delete_menus',
                'table_name' => 'menus',
                'created_at' => '2021-09-19 18:19:19',
                'updated_at' => '2021-09-19 18:19:19',
            ),
            10 => 
            array (
                'id' => 11,
                'key' => 'browse_roles',
                'table_name' => 'roles',
                'created_at' => '2021-09-19 18:19:19',
                'updated_at' => '2021-09-19 18:19:19',
            ),
            11 => 
            array (
                'id' => 12,
                'key' => 'read_roles',
                'table_name' => 'roles',
                'created_at' => '2021-09-19 18:19:19',
                'updated_at' => '2021-09-19 18:19:19',
            ),
            12 => 
            array (
                'id' => 13,
                'key' => 'edit_roles',
                'table_name' => 'roles',
                'created_at' => '2021-09-19 18:19:19',
                'updated_at' => '2021-09-19 18:19:19',
            ),
            13 => 
            array (
                'id' => 14,
                'key' => 'add_roles',
                'table_name' => 'roles',
                'created_at' => '2021-09-19 18:19:19',
                'updated_at' => '2021-09-19 18:19:19',
            ),
            14 => 
            array (
                'id' => 15,
                'key' => 'delete_roles',
                'table_name' => 'roles',
                'created_at' => '2021-09-19 18:19:19',
                'updated_at' => '2021-09-19 18:19:19',
            ),
            15 => 
            array (
                'id' => 16,
                'key' => 'browse_users',
                'table_name' => 'users',
                'created_at' => '2021-09-19 18:19:19',
                'updated_at' => '2021-09-19 18:19:19',
            ),
            16 => 
            array (
                'id' => 17,
                'key' => 'read_users',
                'table_name' => 'users',
                'created_at' => '2021-09-19 18:19:19',
                'updated_at' => '2021-09-19 18:19:19',
            ),
            17 => 
            array (
                'id' => 18,
                'key' => 'edit_users',
                'table_name' => 'users',
                'created_at' => '2021-09-19 18:19:19',
                'updated_at' => '2021-09-19 18:19:19',
            ),
            18 => 
            array (
                'id' => 19,
                'key' => 'add_users',
                'table_name' => 'users',
                'created_at' => '2021-09-19 18:19:19',
                'updated_at' => '2021-09-19 18:19:19',
            ),
            19 => 
            array (
                'id' => 20,
                'key' => 'delete_users',
                'table_name' => 'users',
                'created_at' => '2021-09-19 18:19:19',
                'updated_at' => '2021-09-19 18:19:19',
            ),
            20 => 
            array (
                'id' => 21,
                'key' => 'browse_settings',
                'table_name' => 'settings',
                'created_at' => '2021-09-19 18:19:19',
                'updated_at' => '2021-09-19 18:19:19',
            ),
            21 => 
            array (
                'id' => 22,
                'key' => 'read_settings',
                'table_name' => 'settings',
                'created_at' => '2021-09-19 18:19:19',
                'updated_at' => '2021-09-19 18:19:19',
            ),
            22 => 
            array (
                'id' => 23,
                'key' => 'edit_settings',
                'table_name' => 'settings',
                'created_at' => '2021-09-19 18:19:19',
                'updated_at' => '2021-09-19 18:19:19',
            ),
            23 => 
            array (
                'id' => 24,
                'key' => 'add_settings',
                'table_name' => 'settings',
                'created_at' => '2021-09-19 18:19:19',
                'updated_at' => '2021-09-19 18:19:19',
            ),
            24 => 
            array (
                'id' => 25,
                'key' => 'delete_settings',
                'table_name' => 'settings',
                'created_at' => '2021-09-19 18:19:19',
                'updated_at' => '2021-09-19 18:19:19',
            ),
            25 => 
            array (
                'id' => 26,
                'key' => 'browse_blogs',
                'table_name' => 'blogs',
                'created_at' => '2021-09-19 22:25:37',
                'updated_at' => '2021-09-19 22:25:37',
            ),
            26 => 
            array (
                'id' => 27,
                'key' => 'read_blogs',
                'table_name' => 'blogs',
                'created_at' => '2021-09-19 22:25:37',
                'updated_at' => '2021-09-19 22:25:37',
            ),
            27 => 
            array (
                'id' => 28,
                'key' => 'edit_blogs',
                'table_name' => 'blogs',
                'created_at' => '2021-09-19 22:25:37',
                'updated_at' => '2021-09-19 22:25:37',
            ),
            28 => 
            array (
                'id' => 29,
                'key' => 'add_blogs',
                'table_name' => 'blogs',
                'created_at' => '2021-09-19 22:25:37',
                'updated_at' => '2021-09-19 22:25:37',
            ),
            29 => 
            array (
                'id' => 30,
                'key' => 'delete_blogs',
                'table_name' => 'blogs',
                'created_at' => '2021-09-19 22:25:37',
                'updated_at' => '2021-09-19 22:25:37',
            ),
            30 => 
            array (
                'id' => 31,
                'key' => 'browse_repositorios',
                'table_name' => 'repositorios',
                'created_at' => '2021-09-23 16:24:45',
                'updated_at' => '2021-09-23 16:24:45',
            ),
            31 => 
            array (
                'id' => 32,
                'key' => 'read_repositorios',
                'table_name' => 'repositorios',
                'created_at' => '2021-09-23 16:24:45',
                'updated_at' => '2021-09-23 16:24:45',
            ),
            32 => 
            array (
                'id' => 33,
                'key' => 'edit_repositorios',
                'table_name' => 'repositorios',
                'created_at' => '2021-09-23 16:24:45',
                'updated_at' => '2021-09-23 16:24:45',
            ),
            33 => 
            array (
                'id' => 34,
                'key' => 'add_repositorios',
                'table_name' => 'repositorios',
                'created_at' => '2021-09-23 16:24:45',
                'updated_at' => '2021-09-23 16:24:45',
            ),
            34 => 
            array (
                'id' => 35,
                'key' => 'delete_repositorios',
                'table_name' => 'repositorios',
                'created_at' => '2021-09-23 16:24:45',
                'updated_at' => '2021-09-23 16:24:45',
            ),
            35 => 
            array (
                'id' => 36,
                'key' => 'browse_quehacemos',
                'table_name' => 'quehacemos',
                'created_at' => '2021-09-23 17:27:59',
                'updated_at' => '2021-09-23 17:27:59',
            ),
            36 => 
            array (
                'id' => 37,
                'key' => 'read_quehacemos',
                'table_name' => 'quehacemos',
                'created_at' => '2021-09-23 17:27:59',
                'updated_at' => '2021-09-23 17:27:59',
            ),
            37 => 
            array (
                'id' => 38,
                'key' => 'edit_quehacemos',
                'table_name' => 'quehacemos',
                'created_at' => '2021-09-23 17:27:59',
                'updated_at' => '2021-09-23 17:27:59',
            ),
            38 => 
            array (
                'id' => 39,
                'key' => 'add_quehacemos',
                'table_name' => 'quehacemos',
                'created_at' => '2021-09-23 17:27:59',
                'updated_at' => '2021-09-23 17:27:59',
            ),
            39 => 
            array (
                'id' => 40,
                'key' => 'delete_quehacemos',
                'table_name' => 'quehacemos',
                'created_at' => '2021-09-23 17:27:59',
                'updated_at' => '2021-09-23 17:27:59',
            ),
            40 => 
            array (
                'id' => 41,
                'key' => 'browse_clientes',
                'table_name' => 'clientes',
                'created_at' => '2021-09-23 17:29:22',
                'updated_at' => '2021-09-23 17:29:22',
            ),
            41 => 
            array (
                'id' => 42,
                'key' => 'read_clientes',
                'table_name' => 'clientes',
                'created_at' => '2021-09-23 17:29:22',
                'updated_at' => '2021-09-23 17:29:22',
            ),
            42 => 
            array (
                'id' => 43,
                'key' => 'edit_clientes',
                'table_name' => 'clientes',
                'created_at' => '2021-09-23 17:29:22',
                'updated_at' => '2021-09-23 17:29:22',
            ),
            43 => 
            array (
                'id' => 44,
                'key' => 'add_clientes',
                'table_name' => 'clientes',
                'created_at' => '2021-09-23 17:29:22',
                'updated_at' => '2021-09-23 17:29:22',
            ),
            44 => 
            array (
                'id' => 45,
                'key' => 'delete_clientes',
                'table_name' => 'clientes',
                'created_at' => '2021-09-23 17:29:22',
                'updated_at' => '2021-09-23 17:29:22',
            ),
            45 => 
            array (
                'id' => 46,
                'key' => 'browse_equipos',
                'table_name' => 'equipos',
                'created_at' => '2021-09-23 17:32:25',
                'updated_at' => '2021-09-23 17:32:25',
            ),
            46 => 
            array (
                'id' => 47,
                'key' => 'read_equipos',
                'table_name' => 'equipos',
                'created_at' => '2021-09-23 17:32:25',
                'updated_at' => '2021-09-23 17:32:25',
            ),
            47 => 
            array (
                'id' => 48,
                'key' => 'edit_equipos',
                'table_name' => 'equipos',
                'created_at' => '2021-09-23 17:32:25',
                'updated_at' => '2021-09-23 17:32:25',
            ),
            48 => 
            array (
                'id' => 49,
                'key' => 'add_equipos',
                'table_name' => 'equipos',
                'created_at' => '2021-09-23 17:32:25',
                'updated_at' => '2021-09-23 17:32:25',
            ),
            49 => 
            array (
                'id' => 50,
                'key' => 'delete_equipos',
                'table_name' => 'equipos',
                'created_at' => '2021-09-23 17:32:25',
                'updated_at' => '2021-09-23 17:32:25',
            ),
            50 => 
            array (
                'id' => 51,
                'key' => 'browse_mainbanners',
                'table_name' => 'mainbanners',
                'created_at' => '2021-09-23 17:38:54',
                'updated_at' => '2021-09-23 17:38:54',
            ),
            51 => 
            array (
                'id' => 52,
                'key' => 'read_mainbanners',
                'table_name' => 'mainbanners',
                'created_at' => '2021-09-23 17:38:54',
                'updated_at' => '2021-09-23 17:38:54',
            ),
            52 => 
            array (
                'id' => 53,
                'key' => 'edit_mainbanners',
                'table_name' => 'mainbanners',
                'created_at' => '2021-09-23 17:38:54',
                'updated_at' => '2021-09-23 17:38:54',
            ),
            53 => 
            array (
                'id' => 54,
                'key' => 'add_mainbanners',
                'table_name' => 'mainbanners',
                'created_at' => '2021-09-23 17:38:54',
                'updated_at' => '2021-09-23 17:38:54',
            ),
            54 => 
            array (
                'id' => 55,
                'key' => 'delete_mainbanners',
                'table_name' => 'mainbanners',
                'created_at' => '2021-09-23 17:38:54',
                'updated_at' => '2021-09-23 17:38:54',
            ),
            55 => 
            array (
                'id' => 56,
                'key' => 'browse_mainpages',
                'table_name' => 'mainpages',
                'created_at' => '2021-09-23 17:45:05',
                'updated_at' => '2021-09-23 17:45:05',
            ),
            56 => 
            array (
                'id' => 57,
                'key' => 'read_mainpages',
                'table_name' => 'mainpages',
                'created_at' => '2021-09-23 17:45:05',
                'updated_at' => '2021-09-23 17:45:05',
            ),
            57 => 
            array (
                'id' => 58,
                'key' => 'edit_mainpages',
                'table_name' => 'mainpages',
                'created_at' => '2021-09-23 17:45:05',
                'updated_at' => '2021-09-23 17:45:05',
            ),
            58 => 
            array (
                'id' => 59,
                'key' => 'add_mainpages',
                'table_name' => 'mainpages',
                'created_at' => '2021-09-23 17:45:05',
                'updated_at' => '2021-09-23 17:45:05',
            ),
            59 => 
            array (
                'id' => 60,
                'key' => 'delete_mainpages',
                'table_name' => 'mainpages',
                'created_at' => '2021-09-23 17:45:05',
                'updated_at' => '2021-09-23 17:45:05',
            ),
            60 => 
            array (
                'id' => 61,
                'key' => 'browse_anuncios',
                'table_name' => 'anuncios',
                'created_at' => '2021-09-24 23:17:19',
                'updated_at' => '2021-09-24 23:17:19',
            ),
            61 => 
            array (
                'id' => 62,
                'key' => 'read_anuncios',
                'table_name' => 'anuncios',
                'created_at' => '2021-09-24 23:17:19',
                'updated_at' => '2021-09-24 23:17:19',
            ),
            62 => 
            array (
                'id' => 63,
                'key' => 'edit_anuncios',
                'table_name' => 'anuncios',
                'created_at' => '2021-09-24 23:17:19',
                'updated_at' => '2021-09-24 23:17:19',
            ),
            63 => 
            array (
                'id' => 64,
                'key' => 'add_anuncios',
                'table_name' => 'anuncios',
                'created_at' => '2021-09-24 23:17:19',
                'updated_at' => '2021-09-24 23:17:19',
            ),
            64 => 
            array (
                'id' => 65,
                'key' => 'delete_anuncios',
                'table_name' => 'anuncios',
                'created_at' => '2021-09-24 23:17:19',
                'updated_at' => '2021-09-24 23:17:19',
            ),
            65 => 
            array (
                'id' => 66,
                'key' => 'browse_mensajes',
                'table_name' => 'mensajes',
                'created_at' => '2021-09-29 16:37:36',
                'updated_at' => '2021-09-29 16:37:36',
            ),
            66 => 
            array (
                'id' => 67,
                'key' => 'read_mensajes',
                'table_name' => 'mensajes',
                'created_at' => '2021-09-29 16:37:36',
                'updated_at' => '2021-09-29 16:37:36',
            ),
            67 => 
            array (
                'id' => 68,
                'key' => 'edit_mensajes',
                'table_name' => 'mensajes',
                'created_at' => '2021-09-29 16:37:36',
                'updated_at' => '2021-09-29 16:37:36',
            ),
            68 => 
            array (
                'id' => 69,
                'key' => 'add_mensajes',
                'table_name' => 'mensajes',
                'created_at' => '2021-09-29 16:37:36',
                'updated_at' => '2021-09-29 16:37:36',
            ),
            69 => 
            array (
                'id' => 70,
                'key' => 'delete_mensajes',
                'table_name' => 'mensajes',
                'created_at' => '2021-09-29 16:37:36',
                'updated_at' => '2021-09-29 16:37:36',
            ),
            70 => 
            array (
                'id' => 71,
                'key' => 'browse_terminos_condiciones',
                'table_name' => 'terminos_condiciones',
                'created_at' => '2021-09-29 16:55:30',
                'updated_at' => '2021-09-29 16:55:30',
            ),
            71 => 
            array (
                'id' => 72,
                'key' => 'read_terminos_condiciones',
                'table_name' => 'terminos_condiciones',
                'created_at' => '2021-09-29 16:55:30',
                'updated_at' => '2021-09-29 16:55:30',
            ),
            72 => 
            array (
                'id' => 73,
                'key' => 'edit_terminos_condiciones',
                'table_name' => 'terminos_condiciones',
                'created_at' => '2021-09-29 16:55:30',
                'updated_at' => '2021-09-29 16:55:30',
            ),
            73 => 
            array (
                'id' => 74,
                'key' => 'add_terminos_condiciones',
                'table_name' => 'terminos_condiciones',
                'created_at' => '2021-09-29 16:55:30',
                'updated_at' => '2021-09-29 16:55:30',
            ),
            74 => 
            array (
                'id' => 75,
                'key' => 'delete_terminos_condiciones',
                'table_name' => 'terminos_condiciones',
                'created_at' => '2021-09-29 16:55:30',
                'updated_at' => '2021-09-29 16:55:30',
            ),
            75 => 
            array (
                'id' => 76,
                'key' => 'browse_dominios',
                'table_name' => 'dominios',
                'created_at' => '2021-10-26 19:07:48',
                'updated_at' => '2021-10-26 19:07:48',
            ),
            76 => 
            array (
                'id' => 77,
                'key' => 'read_dominios',
                'table_name' => 'dominios',
                'created_at' => '2021-10-26 19:07:48',
                'updated_at' => '2021-10-26 19:07:48',
            ),
            77 => 
            array (
                'id' => 78,
                'key' => 'edit_dominios',
                'table_name' => 'dominios',
                'created_at' => '2021-10-26 19:07:48',
                'updated_at' => '2021-10-26 19:07:48',
            ),
            78 => 
            array (
                'id' => 79,
                'key' => 'add_dominios',
                'table_name' => 'dominios',
                'created_at' => '2021-10-26 19:07:48',
                'updated_at' => '2021-10-26 19:07:48',
            ),
            79 => 
            array (
                'id' => 80,
                'key' => 'delete_dominios',
                'table_name' => 'dominios',
                'created_at' => '2021-10-26 19:07:48',
                'updated_at' => '2021-10-26 19:07:48',
            ),
            80 => 
            array (
                'id' => 81,
                'key' => 'browse_portafolios',
                'table_name' => 'portafolios',
                'created_at' => '2021-10-26 19:12:13',
                'updated_at' => '2021-10-26 19:12:13',
            ),
            81 => 
            array (
                'id' => 82,
                'key' => 'read_portafolios',
                'table_name' => 'portafolios',
                'created_at' => '2021-10-26 19:12:13',
                'updated_at' => '2021-10-26 19:12:13',
            ),
            82 => 
            array (
                'id' => 83,
                'key' => 'edit_portafolios',
                'table_name' => 'portafolios',
                'created_at' => '2021-10-26 19:12:13',
                'updated_at' => '2021-10-26 19:12:13',
            ),
            83 => 
            array (
                'id' => 84,
                'key' => 'add_portafolios',
                'table_name' => 'portafolios',
                'created_at' => '2021-10-26 19:12:13',
                'updated_at' => '2021-10-26 19:12:13',
            ),
            84 => 
            array (
                'id' => 85,
                'key' => 'delete_portafolios',
                'table_name' => 'portafolios',
                'created_at' => '2021-10-26 19:12:13',
                'updated_at' => '2021-10-26 19:12:13',
            ),
            85 => 
            array (
                'id' => 91,
                'key' => 'browse_productos',
                'table_name' => 'productos',
                'created_at' => '2021-10-26 19:53:51',
                'updated_at' => '2021-10-26 19:53:51',
            ),
            86 => 
            array (
                'id' => 92,
                'key' => 'read_productos',
                'table_name' => 'productos',
                'created_at' => '2021-10-26 19:53:51',
                'updated_at' => '2021-10-26 19:53:51',
            ),
            87 => 
            array (
                'id' => 93,
                'key' => 'edit_productos',
                'table_name' => 'productos',
                'created_at' => '2021-10-26 19:53:51',
                'updated_at' => '2021-10-26 19:53:51',
            ),
            88 => 
            array (
                'id' => 94,
                'key' => 'add_productos',
                'table_name' => 'productos',
                'created_at' => '2021-10-26 19:53:51',
                'updated_at' => '2021-10-26 19:53:51',
            ),
            89 => 
            array (
                'id' => 95,
                'key' => 'delete_productos',
                'table_name' => 'productos',
                'created_at' => '2021-10-26 19:53:51',
                'updated_at' => '2021-10-26 19:53:51',
            ),
            90 => 
            array (
                'id' => 96,
                'key' => 'browse_licencias',
                'table_name' => 'licencias',
                'created_at' => '2022-02-25 21:13:56',
                'updated_at' => '2022-02-25 21:13:56',
            ),
            91 => 
            array (
                'id' => 97,
                'key' => 'read_licencias',
                'table_name' => 'licencias',
                'created_at' => '2022-02-25 21:13:56',
                'updated_at' => '2022-02-25 21:13:56',
            ),
            92 => 
            array (
                'id' => 98,
                'key' => 'edit_licencias',
                'table_name' => 'licencias',
                'created_at' => '2022-02-25 21:13:56',
                'updated_at' => '2022-02-25 21:13:56',
            ),
            93 => 
            array (
                'id' => 99,
                'key' => 'add_licencias',
                'table_name' => 'licencias',
                'created_at' => '2022-02-25 21:13:56',
                'updated_at' => '2022-02-25 21:13:56',
            ),
            94 => 
            array (
                'id' => 100,
                'key' => 'delete_licencias',
                'table_name' => 'licencias',
                'created_at' => '2022-02-25 21:13:56',
                'updated_at' => '2022-02-25 21:13:56',
            ),
        ));
        
        
    }
}